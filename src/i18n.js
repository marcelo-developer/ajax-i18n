var i18n = {
	fetch: function(lang) {
		var srcFolder = $('<a>').attr('href', $('meta[name="i18n-src"]').attr('content')).prop('href');
		var srcFile = srcFolder + '/' + lang + '.json';
		$.ajax({
			url: srcFile,
			type: 'GET',
			dataType: 'json',
			success: i18n.bind
		});
	},
	bind: function(data, status, jqXHR) {
		$.each(data, function(key, value) {
			$('[data-i18n="' + key + '"]').html(value);
		});
	},
	functionByName: function(name, args) {
		var namespaces = name.split('.');
		var context = window;
		for (var i = 0; i < namespaces.length-1; i++)
			context = context[namespaces[i]];
		return context[namespaces[namespaces.length-1]].apply(context, args);
	}
}

$(function() {
	var triggers = $('[data-i18n-event]');
	var defaultLang = $('meta[name="i18n-default"]');
	if (defaultLang)
		i18n.fetch($(defaultLang).attr('content'));
	$(triggers).each(function(index, element) {
		var event = $(element).attr('data-i18n-event');
		$(element).on(event, function() {
			var lang = $(element).attr('data-i18n-lang');
			var attr = $(element).attr('data-i18n-attr');
			var func = $(element).attr('data-i18n-func');
			if (func)
				lang = i18n.functionByName(func, [$(element)]);
			if (attr)
				lang = $(element).prop(attr);
			i18n.fetch(lang);
		});
	})
});